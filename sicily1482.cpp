//题目分析:
//PE了三次，那个格式问题好无语。
//其实到现在我也没弄懂题目字面表达和这个格式是怎么联系起来的。
//“两个相邻测试数据间用一个空行隔开。”
//回归重点，我的思路是用冒泡法，
//由于只要最大的5个，所以只要冒5次就成，
//反正没TL，也是0.00秒的事，
//冒泡思路跟题目一样，
//先比较总成绩，再比较语文，再比较学号，
//考虑过用结构体，感觉太浪费内存空间。

//题目网址:http://soj.me/1482

#include <iostream>

using namespace std;

void swap(int a[3], int b[3])
{
    int t[3];
    t[0] = a[0], t[1] = a[1], t[2] = a[2];
    a[0] = b[0], a[1] = b[1], a[2] = b[2];
    b[0] = t[0], b[1] = t[1], b[2] = t[2];
    return;
}

void bubbleSort(int arr[10001][3], int len)
{
    int k = 1;
    for (int i = 1; i <= 5; i++) {
        for (int j = 1; j <= len - k; j++) {
            if (arr[j][0] > arr[j+1][0]) {
                swap(arr[j], arr[j+1]);
            } else if (arr[j][0] == arr[j+1][0]) {
                if (arr[j][1] > arr[j+1][1]) {
                    swap(arr[j], arr[j+1]);
                } else if (arr[j][1] == arr[j+1][1]) {
                    if(arr[j][2] < arr[j+1][2]) {
                        swap(arr[j], arr[j+1]);
                    }
                }
            }
        }
        k++;
    }
    return;
}

int main()
{
    int student[10001][3] = {0};
    int chinese;
    int math;
    int english;
    int n;
    bool flag = false;
    while (cin >> n) {
        if (flag) {
            cout << endl;
        }
        flag = true;

        for (int i = 1; i <= n; i++) {
            cin >> chinese >> math >> english;
            student[i][0] = chinese + math + english;
            student[i][1] = chinese;
            student[i][2] = i;
        }
        
        bubbleSort(student, n);

        for (int i = n ; i >= n - 4; i--) {
            cout << student[i][2] << ' ' << student[i][0] << endl;
        }
    }
    return 0;
}                                 